const pkg = require('./package');
const webpack = require('webpack');
const appConfig = require('./config/app.js');

module.exports = {
	mode: 'spa',

	/*
	** Headers of the page
	*/
	head: {
		title: 'Pokémon',
		meta: [
			{charset: 'utf-8'},
			{name: 'viewport', content: 'width=device-width, initial-scale=1'},
			{hid: 'description', name: 'description', content: pkg.description}
		],
		link: [
			{rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
		]
	},

	env: {
		...appConfig,
	},

	/*
	** Customize the progress-bar color
	*/
	loading: {
		color: 'green',
	},

	/*
	** Global CSS
	*/
	css: [
		'~/assets/scss/app.scss',
	],

	/*
	** Plugins to load before mounting the App
	*/
	plugins: [
		'~/plugins/axios'
	],

	/*
	** Nuxt.js modules
	*/
	modules: [
		// Doc: https://axios.nuxtjs.org/usage
		'@nuxtjs/axios',
	],

	/**
	 * Change build directory name
	 */
	buildDir: '.pokemon',

	/*
	** Build configuration
	*/
	build: {
		/**
		 * Extract css into files for better caching
		 */
		extractCSS: true,

		/**
		 * Change public path
		 */
		publicPath: '/_pokemon/',
	}
};
