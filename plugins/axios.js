export default function ({$axios, redirect}) {
	$axios.onRequest(config => {
		config.headers.common['Accept'] = 'application/json';
		config.headers.common['Content-Type'] = 'application/json';
	});
}
